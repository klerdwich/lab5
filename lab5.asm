; It is assumed that the following connections on the board are made:
;RO-C3 -> PC0-PC7
;LCD D0-D7 -> PA0-PA7
;LCD RW-RS -> PB0-PB3
;Mot -> PB7
;OpE -> PB6
;OpD -> PD0
; These ports can be changed if required by replacing all references to the ports with a
; different port. This means replacing occurences of DDRx, PORTx and PINx.


;Y+1 is used for data or number to be displayed on the LCD
;Y+2 counter 1 
; Y+3 counter 2
; Y+4 Counter 3


.include "m64def.inc"
.def temp =r16
.def temp2 =r17
.def row =r18
.def col =r19
.def mask =r20

.def rps = r21
.def rps2=r22
.def num = r23
.def c = r24
.def c2=r25
.def speed = r26



;setting up the interrupt vector


;Stack variables
.equ DATA = 1
.equ COUNTER1 = 2
.equ COUNTER2 = 3
.equ COUNTER3 = 4



.equ PORTCDIR = 0xF0
.equ INITCOLMASK = 0xEF
.equ INITROWMASK = 0x01
.equ ROWMASK = 0x0F



;LCD protocol control bits
.equ LCD_RS = 3
.equ LCD_RW = 1
.equ LCD_E = 2
;LCD functions
.equ LCD_FUNC_SET = 0b00110000
.equ LCD_DISP_OFF = 0b00001000
.equ LCD_DISP_CLR = 0b00000001
.equ LCD_DISP_ON = 0b00001100
.equ LCD_ENTRY_SET = 0b00000100
.equ LCD_ADDR_SET = 0b10000000
;LCD function bits and constants
.equ LCD_BF = 7
.equ LCD_N = 3
.equ LCD_F = 2
.equ LCD_ID = 1
.equ LCD_S = 0
.equ LCD_C = 1
.equ LCD_B = 0
.equ LCD_LINE1 = 0
.equ LCD_LINE2 = 0x40




.MACRO delay
pop temp2
pop temp
loop:

subi temp, 1
sbci temp2, 0
nop
nop
nop
nop
brne loop
; taken branch takes two cycles.
; one loop time is 8 cycles = ~1.08us
.ENDMACRO


jmp RESET
jmp EXT_INT0 ; IRQ0 Handler
jmp Default ;DEFINE what the interupt do
jmp Default ; IRQ2 Handler
jmp Default ; IRQ3 Handler
jmp Default; IRQ4 Handler
jmp Default ; IRQ5 Handler
jmp Default ; IRQ6 Handler	
jmp Default ; IRQ7 Handler
jmp Default ; Timer2 Compare Handler
jmp Default ; Timer2 Overflow Handler
jmp Default ; Timer1 Capture Handler
jmp Default ; Timer1 CompareA Handler
jmp Default ; Timer1 CompareB Handler
jmp Default ; Timer1 Overflow Handler
jmp Default ; Timer0 Compare Handler
jmp Timer0 ; Timer0 Overflow Handler

Default: reti

RESET:
;Debug for display number
;rjmp displayNumber
;end debug
ldi temp, low(RAMEND - 6)  ;stack and Y pointers now point to -6 from RAMEND
out SPL, temp
ldi temp, high(RAMEND - 6) 
out SPH, temp

ldi temp, PORTCDIR ; columns are outputs, rows are inputs
out DDRC, temp

; main keeps scanning the keypad to find which key is pressed.
ldi rps,0

ser temp
out DDRB, temp
nop
nop
; Bit 4 will function as OC2.
ldi temp, 0x59
mov speed,temp
; the value controls the PWM duty cycle
out OCR2, temp
; Set the Timer2 to Phase Correct PWM mode.
ldi temp, (1<< WGM20)|(1<<COM21)|(1<<CS20)
out TCCR2, temp
nop
nop

;*********For External interrupt 0 ************
ldi temp, (1 << ISC01);setting the interrupts for falling edge
sts EICRA, temp                       ;storing them into EICRB
in temp, EIMSK                        ;taking the values inside the EIMSK  
ori temp, (1<<INT0) ;enabling pinE0 for interrupt 0
out EIMSK, temp
nop
nop


ldi temp,255
out PORTB,temp
nop

nop

rcall lcd_init
sei ; enabling the global interrupt..(MUST)



jmp main

;dobble dabble
;****************************NUMBER CONVERSION****************************************

displayNumber:
ldi c,0
ldi c2,0
ldd num, Y+DATA
cpi num, 10 ; if num >= 10
brsh TenLoop
rcall printDigits
ret

printDigits:
ldi temp, 48
add num, temp
std Y+DATA,num
rcall lcd_write_data
rcall lcd_wait_busy
ret
printTens:
cpi c, 10
brge HundredLoop
brvs HundredLoop
ldi temp, 48
add c, temp
std Y + DATA,c
rcall lcd_write_data
rcall lcd_wait_busy
rcall printDigits
ret

printHundreds:
ldi temp, 48
add c2, temp
std Y + DATA,c2
rcall lcd_write_data
rcall lcd_wait_busy
rcall printTens
ret

HundredLoop:
subi c,10
inc c2
cpi c,10 ; if num >= 10
brsh HundredLoop
jmp printHundreds

TenLoop:
subi num,10
inc c
cpi num,10 ; if num >= 10
brsh TenLoop
jmp printTens


;********************************************************************************
;						Logic for External Interupt 0 (Detector)
;*******************************************************************************

EXT_INT0:
push temp
push r29                 ; Save all conflict registers in the prologue.
push r28
in temp, SREG
push temp                 ; Prologue ends.

inc rps
nop

pop temp                  ; Epilogue starts;
out SREG, temp           ; Restore all conflict registers from the stack.
pop r28
pop r29
pop temp

reti

;********************************************************************************
;						Logic for Timer0
;*******************************************************************************



Timer0:                  ; Prologue starts.
push temp
push r29                 ; Save all conflict registers in the prologue.
push r28
in temp, SREG
push temp                 ; Prologue ends.
/**** a counter for 3597 is needed to get one second-- Three counters are used in this example **************/                                          
                         ; 3597  (1 interrupt 278microseconds therefore 3597 interrupts needed for 1 sec)
ldd temp, Y+COUNTER1
cpi temp, 99          ; counting for 97
brne notsecond

ldd temp, Y+COUNTER2
cpi temp, 8         ; counting for 35
brne secondloop          ; jumping into count 100 
; DO STUFF AFTER EACH SECOND HERE:



std Y+DATA, rps

rcall lcd_init
rcall lcd_wait_busy
rcall displayNumber
clr rps


rjmp outmot              ; jump to out put value


outmot: ldi temp, 0
		std Y+COUNTER1, temp ; clearing the counter values after counting 3597 interrupts which gives us one second
        std Y+COUNTER2, temp
        std Y+COUNTER3, temp
        rjmp exit        ; go to exit



notsecond: inc temp   ; if it is not a second, increment the counter
		   std Y+COUNTER1, temp
           rjmp exit

secondloop: ldd temp2, Y+COUNTER3
			inc temp2 ; counting 100 for every 35 times := 35*100 := 3500
			std Y+COUNTER3, temp2
            cpi temp2,100 
            brne exit
		    inc temp
			std Y+COUNTER2, temp
		    ldi temp2,0
			std Y+COUNTER3, temp2
exit: 
pop temp                  ; Epilogue starts;
out SREG, temp           ; Restore all conflict registers from the stack.
pop r28
pop r29
pop temp
reti                     ; Return from the interrupt.



;********************************************************************************
;						Logic for LCD
;*******************************************************************************


;Function lcd_write_com: Write a command to the LCD. The data reg stores the value to be written.
lcd_write_com:
push temp
out PORTA, temp ; set the data port's value up
clr temp
out PORTB, temp ; RS = 0, RW = 0 for a command write
nop ; delay to meet timing (Set up time)
sbi PORTB, LCD_E ; turn on the enable pin
nop ; delay to meet timing (Enable pulse width)
nop
nop
cbi PORTB, LCD_E ; turn off the enable pin
nop ; delay to meet timing (Enable cycle time)
nop
nop
pop temp
ret
;Function lcd_write_data: Write a character to the LCD. The data reg stores the value to be written.

lcd_write_data:
push temp
ldd temp, Y+DATA
out PORTA, temp ; set the data port's value up
ldi temp, 1 << LCD_RS
out PORTB, temp ; RS = 1, RW = 0 for a data write
nop ; delay to meet timing (Set up time)
sbi PORTB, LCD_E ; turn on the enable pin
nop ; delay to meet timing (Enable pulse width)
nop
nop
cbi PORTB, LCD_E ; turn off the enable pin
nop ; delay to meet timing (Enable cycle time)
nop
nop
pop temp
ret

;Function lcd_wait_busy: Read the LCD busy flag until it reads as not busy.
lcd_wait_busy:
clr temp
out DDRA, temp ; Make PORTA be an input port for now
out PORTA, temp
ldi temp, 1 << LCD_RW
out PORTB, temp ; RS = 0, RW = 1 for a command port read
busy_loop:
nop ; delay to meet timing (Set up time / Enable cycle time)
sbi PORTB, LCD_E ; turn on the enable pin
nop ; delay to meet timing (Data delay time)
nop
nop
in temp, PINA ; read value from LCD
cbi PORTB, LCD_E ; turn off the enable pin
sbrc temp, LCD_BF ; if the busy flag is set
rjmp busy_loop ; repeat command read
clr temp ; else
out PORTB, temp ; turn off read mode,
ser temp
out DDRA, temp ; make PORTA an output port again
ret ; and return
; Function delay: Pass a number in registers r18:r19 to indicate how many microseconds
; must be delayed. Actua6l delay will be slightly greater (~1.08us*r18:r19).
; r18:r19 are altered in this function.
; Code is omitted



;Function lcd_init Initialisation function for LCD.
lcd_init:
push temp
push r28
push r29
ser temp
out DDRA, temp ; PORTA, the data port is usually all otuputs
out DDRB, temp ; PORTB, the control port is always all outputs

ldi temp, low(15000)   ;load delay
ldi temp2, high(15000)
push temp
push temp2
delay ; delay for > 15ms

; Function set command with N = 1 and F = 0
ldi temp, LCD_FUNC_SET | (1 << LCD_N)

rcall lcd_write_com ; 1st Function set command with 2 lines and 5*7 font


ldi temp, low(4100)   ;load delay
ldi temp2, high(4100)
push temp
push temp2
delay ; delay for > 4.1ms


ldi temp, low(100)   ;load delay
ldi temp2, high(100)
push temp
push temp2
delay ; delay for > 100us

rcall lcd_write_com ; 3rd Function set command with 2 lines and 5*7 font
rcall lcd_write_com ; Final Function set command with 2 lines and 5*7 font
rcall lcd_wait_busy ; Wait until the LCD is ready
ldi temp, LCD_DISP_OFF
rcall lcd_write_com ; Turn Display off
rcall lcd_wait_busy ; Wait until the LCD is ready
ldi temp, LCD_DISP_CLR
rcall lcd_write_com ; Clear Display
rcall lcd_wait_busy ; Wait until the LCD is ready
; Entry set command with I/D = 1 and S = 0
ldi temp, LCD_ENTRY_SET | (1 << LCD_ID)
rcall lcd_write_com ; Set Entry mode: Increment = yes and Shift = no
rcall lcd_wait_busy ; Wait until the LCD is ready
; Display on command with C = 0 and B = 1
ldi temp, LCD_DISP_ON | (1 << LCD_C)

rcall lcd_write_com ; Trun Display on with a cursor that doesn't blink
pop r29
pop r28
pop temp
ret

;*****************************************************************************************
; 							Logic for keypad and main
;*****************************************************************************************
  
main:
clr temp

ldi temp, 0b00000010 ;
out TCCR0, temp ; Prescaling value=8 ;256*8/7.3728( Frequency of the clock 7.3728MHz, for the overflow it should go for 256 times)
ldi temp, 1<<TOIE0 ; =278 microseconds
out TIMSK, temp ; T/C0 interrupt enable


ldi mask, INITCOLMASK ; initial column mask
clr col ; initial column
colloop:
out PORTC, mask ; set column to mask value
; (sets column 0 off)
ldi temp, 0xFF ; implement a delay so the
; hardware can stabilize

ser temp
out DDRB, temp
ldi temp,255
out PORTB,temp

delayKeypad:
dec temp
brne delayKeypad
rcall buttonDelay
rcall buttonDelay
in temp, PINC ; read PORTB
andi temp, ROWMASK ; read only the row bits
cpi temp, 0xF ; check if any rows are grounded
breq nextcol ; if not go to the next column
ldi mask, INITROWMASK ; initialise row check
clr row ; initial row


rowloop:
mov temp2, temp
and temp2, mask ; check masked bit
brne skipconv ; if the result is non-zero,
; we need to look again

rcall convert ; if bit is clear, convert the bitcode
jmp main ; and start again


buttonDelay:
push temp	;store the temp values
push temp2
ldi temp, low(4100)   ;load delay
ldi temp2, high(4100)
push temp
push temp2
delay ; delay for > 4.1ms
pop temp2
pop temp

ret


skipconv:
inc row ; else move to the next row
lsl mask ; shift the mask to the next bit
jmp rowloop
nextcol:
cpi col, 3 ; check if we’re on the last column
breq main ; if so, no buttons were pushed,
; so start again.

sec ; else shift the column mask:
; We must set the carry bit
rol mask ; and then rotate left by a bit,
; shifting the carry into
; bit zero. We need this to make
; sure all the rows have
; pull-up resistors
inc col ; increment column value
jmp colloop ; and check the next column
; convert function converts the row and column given to a
; binary number and also outputs the value to PORTC.
; Inputs come from registers row and col and output is in
; temp.

convert:
;numbers:
mov temp, row ; otherwise we have a number (1-9)
lsl temp ; temp = row * 2
add temp, row ; temp = row * 3
add temp, col ; add the column address
; to get the offset from 1
inc temp ; add 1. Value of switch is


convert_end:
	cpi temp, 3
	breq turnOff

	cpi temp, 4
	breq turnOn

	cpi temp, 1
	breq revUp

	cpi temp,2
	breq revDown


	ret
	
turnOn:
push temp
ldi temp, 0x59
mov speed,temp
; the value controls the PWM duty cycle
out OCR2, temp
; Set the Timer2 to Phase Correct PWM mode.
ldi temp, (1<< WGM20)|(1<<COM21)|(1<<CS20)
out TCCR2, temp
pop temp
ret


turnOff:
push temp
ldi temp, 0x00
mov speed,temp
; the value controls the PWM duty cycle
out OCR2, temp
; Set the Timer2 to Phase Correct PWM mode.
ldi temp, (1<< WGM20)|(1<<COM21)|(1<<CS20)
out TCCR2, temp
pop temp

ret


revUp:
push temp
ldi temp, 0x05
add speed, temp

cpi speed, 0xE5
brsh capHigh

resumeHigh:
mov temp,speed
; the value controls the PWM duty cycle
out OCR2, temp
nop
nop
nop
; Set the Timer2 to Phase Correct PWM mode.
ldi temp, (1<< WGM20)|(1<<COM21)|(1<<CS20)
out TCCR2, temp
nop
nop
nop
pop temp

ret

revDown:
push temp
ldi temp, 0x05
sub speed, temp
cpi speed, 0x59
brlo capLow
resumeLow:
mov temp,speed
; the value controls the PWM duty cycle
out OCR2, temp
nop
nop
nop
; Set the Timer2 to Phase Correct PWM mode.
ldi temp, (1<< WGM20)|(1<<COM21)|(1<<CS20)
out TCCR2, temp
nop
nop
nop
pop temp

ret

capHigh:
ldi speed,0xE5
rjmp resumeHigh
ret

capLow:
ldi speed,0x59
rjmp resumeLow
ret

